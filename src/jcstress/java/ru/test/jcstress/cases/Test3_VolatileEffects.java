package ru.test.jcstress.cases;

import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE;
import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE_INTERESTING;

import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.I_Result;

/**
 * ./gradlew :jcstress --tests "Test3_VolatileEffects"
 */
public class Test3_VolatileEffects {

    @JCStressTest
    @Outcome(id = "1", expect = ACCEPTABLE, desc = "Read updated value")
    @Outcome(id = "-1", expect = ACCEPTABLE, desc = "OK, just don't handle flag value")
    @Outcome(id = "0", expect = ACCEPTABLE_INTERESTING, desc = "Read old value")
    @State
    public static class NonVolatileFlag {
        int i = 0;
        boolean flag = false;

        @Actor
        public void actor1() {
            i = 1;
            flag = true;
        }

        @Actor
        public void actor2(I_Result r) {
            if (flag) {
                r.r1 = i;
            } else {
                r.r1 = -1;
            }
        }

    }

    @JCStressTest
    @Outcome(id = "1", expect = ACCEPTABLE, desc = "Read updated value")
    @Outcome(id = "-1", expect = ACCEPTABLE, desc = "OK, just don't handle flag value")
    @Outcome(id = "0", expect = ACCEPTABLE_INTERESTING, desc = "Read old value")
    @State
    public static class VolatileFlag {
        int i = 0;
        volatile boolean flag = false;

        @Actor
        public void actor1() {
            i = 1;
            flag = true;
        }

        @Actor
        public void actor2(I_Result r) {
            if (flag) {
                r.r1 = i;
            } else {
                r.r1 = -1;
            }
        }

    }


    @JCStressTest
    @Outcome(id = "1", expect = ACCEPTABLE, desc = "Read updated value")
    @Outcome(id = "-1", expect = ACCEPTABLE, desc = "OK, just don't handle flag value")
    @Outcome(id = "0", expect = ACCEPTABLE_INTERESTING, desc = "Read old value")
    @State
    public static class VolatileInt {
        volatile int i = 0;
        boolean flag = false;

        @Actor
        public void actor1() {
            i = 1;
            flag = true;
        }

        @Actor
        public void actor2(I_Result r) {
            if (flag) {
                r.r1 = i;
            } else {
                r.r1 = -1;
            }
        }

    }

}