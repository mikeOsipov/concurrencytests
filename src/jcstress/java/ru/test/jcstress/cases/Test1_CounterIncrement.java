package ru.test.jcstress.cases;

import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE;
import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE_INTERESTING;
import static org.openjdk.jcstress.annotations.Expect.FORBIDDEN;

import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.Arbiter;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.I_Result;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * ./gradlew :jcstress --tests "Test1_CounterIncrement"
 */
public class Test1_CounterIncrement {

    @JCStressTest
    @Outcome(id = {"2"}, expect = ACCEPTABLE, desc = "Consistent counting")
    @Outcome(expect = ACCEPTABLE_INTERESTING, desc = "Inconsistent counting")
    @State
    public static class PlainIncrement {
        int counter;

        @Actor
        public void actor1() {
            counter++;
        }

        @Actor
        public void actor2() {
            counter++;
        }

        @Arbiter
        public void arbiter(I_Result r) {
            r.r1 = counter;
        }

    }

    @JCStressTest
    @Outcome(id = {"2"}, expect = ACCEPTABLE, desc = "Consistent counting")
    @Outcome(expect = ACCEPTABLE_INTERESTING, desc = "Inconsistent counting")
    @State
    public static class VolatileIncrement {
        volatile int counter;

        @Actor
        public void actor1() {
            counter++;
        }

        @Actor
        public void actor2() {
            counter++;
        }

        @Arbiter
        public void arbiter(I_Result r) {
            r.r1 = counter;
        }

    }


    @JCStressTest
    @Outcome(id = {"2"}, expect = ACCEPTABLE, desc = "Consistent counting")
    @Outcome(expect = FORBIDDEN, desc = "Inconsistent counting")
    @State
    public static class SynchronizedIncrement {
        final Object lock = new Object();
        int counter;

        @Actor
        public void actor1() {
            synchronized (lock) {
                counter++;
            }
        }

        @Actor
        public void actor2() {
            synchronized (lock) {
                counter++;
            }
        }

        @Arbiter
        public void arbiter(I_Result r) {
            r.r1 = counter;
        }

    }

    @JCStressTest
    @Outcome(id = {"2"}, expect = ACCEPTABLE, desc = "Consistent counting")
    @Outcome(expect = FORBIDDEN, desc = "Inconsistent counting")
    @State
    public static class AtomicIncrement {
        volatile AtomicInteger counter = new AtomicInteger();

        @Actor
        public void actor1() {
            counter.incrementAndGet();
        }

        @Actor
        public void actor2() {
            counter.incrementAndGet();
        }

        @Arbiter
        public void arbiter(I_Result r) {
            r.r1 = counter.get();
        }

    }

}
