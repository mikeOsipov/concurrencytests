package ru.test.jcstress.cases;

import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE;
import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE_INTERESTING;
import static org.openjdk.jcstress.annotations.Expect.FORBIDDEN;

import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.II_Result;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * ./gradlew :jcstress --tests "Test2_AtomicFlag"
 */
public class Test2_AtomicFlag {

    @JCStressTest
    @Outcome(id = {"0, 1", "1, 0"}, expect = ACCEPTABLE, desc = "Enter just one")
    @Outcome(expect = ACCEPTABLE_INTERESTING, desc = "Both enters")
    @State
    public static class VolatileFlag {
        volatile boolean entered = false;

        @Actor
        public void actor1(II_Result r) {
            if (!entered) {
                entered = true;
                r.r1 = 1;
            }
        }

        @Actor
        public void actor2(II_Result r) {
            if (!entered) {
                entered = true;
                r.r2 = 1;
            }
        }

    }

    @JCStressTest
    @Outcome(id = {"0, 1", "1, 0"}, expect = ACCEPTABLE, desc = "Enter just one")
    @Outcome(expect = FORBIDDEN, desc = "Both enters")
    @State
    public static class AtomicFlag {
        AtomicBoolean entered = new AtomicBoolean();

        @Actor
        public void actor1(II_Result r) {
            if (entered.compareAndSet(false, true)) {
                r.r1 = 1;
            }
        }

        @Actor
        public void actor2(II_Result r) {
            if (entered.compareAndSet(false, true)) {
                r.r2 = 1;
            }
        }

    }


}
