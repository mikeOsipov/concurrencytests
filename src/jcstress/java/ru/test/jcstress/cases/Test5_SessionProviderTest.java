package ru.test.jcstress.cases;

import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE;
import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE_INTERESTING;

import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.II_Result;

import ru.ktest.session_provider.SessionProvider;
import ru.ktest.session_provider.SessionProviderDCL;
import ru.ktest.session_provider.SessionProviderSimple;
import ru.ktest.session_provider.SessionProviderSynchronized;
import ru.ktest.session_provider.SessionProviderVolatile;

/**
 * ./gradlew :jcstress --tests "Test5_SessionProviderTest"
 */
public class Test5_SessionProviderTest {

    @JCStressTest
    @Outcome(id = "0, 0", expect = ACCEPTABLE, desc = "Single session requested")
    @Outcome(id = {"1, 0", "0, 1"}, expect = ACCEPTABLE_INTERESTING, desc = "Multiple session requested")
    @State
    public static class TestSessionProviderSimple {
        SessionProvider provider = new SessionProviderSimple();

        @Actor
        public void actor1(II_Result r) {
            r.r1 = provider.getSession().getId();
        }

        @Actor
        public void actor2(II_Result r) {
            r.r2 = provider.getSession().getId();
        }

    }

    @JCStressTest
    @Outcome(id = "0, 0", expect = ACCEPTABLE, desc = "Single session requested")
    @Outcome(id = {"1, 0", "0, 1"}, expect = ACCEPTABLE_INTERESTING, desc = "Multiple session requested")
    @State
    public static class TestSessionProviderVolatile {
        SessionProvider provider = new SessionProviderVolatile();

        @Actor
        public void actor1(II_Result r) {
            r.r1 = provider.getSession().getId();
        }

        @Actor
        public void actor2(II_Result r) {
            r.r2 = provider.getSession().getId();
        }

    }

    @JCStressTest
    @Outcome(id = "0, 0", expect = ACCEPTABLE, desc = "Single session requested")
    @Outcome(id = {"1, 0", "0, 1"}, expect = ACCEPTABLE_INTERESTING, desc = "Multiple session requested")
    @State
    public static class TestSessionProviderSync {
        SessionProvider provider = new SessionProviderSynchronized();

        @Actor
        public void actor1(II_Result r) {
            r.r1 = provider.getSession().getId();
        }

        @Actor
        public void actor2(II_Result r) {
            r.r2 = provider.getSession().getId();
        }

    }

    @JCStressTest
    @Outcome(id = "0, 0", expect = ACCEPTABLE, desc = "Single session requested")
    @Outcome(id = {"1, 0", "0, 1"}, expect = ACCEPTABLE_INTERESTING, desc = "Multiple session requested")
    @State
    public static class TestSessionProviderDCL {
        SessionProvider provider = new SessionProviderDCL();

        @Actor
        public void actor1(II_Result r) {
            r.r1 = provider.getSession().getId();
        }

        @Actor
        public void actor2(II_Result r) {
            r.r2 = provider.getSession().getId();
        }

    }

}