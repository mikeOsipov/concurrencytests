package ru.test.jcstress.cases;

import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE;
import static org.openjdk.jcstress.annotations.Expect.ACCEPTABLE_INTERESTING;

import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Mode;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.Signal;
import org.openjdk.jcstress.annotations.State;

/**
 * ./gradlew :jcstress --tests "Test4_SpinLoop"
 */
public class Test4_SpinLoop {

    @JCStressTest(Mode.Termination)
    @Outcome(id = "TERMINATED", expect = ACCEPTABLE, desc = "Gracefully finished")
    @Outcome(id = "STALE", expect = ACCEPTABLE_INTERESTING, desc = "Test is stuck")
    @State
    public static class PlainSpin {
        boolean ready;

        @Actor
        public void actor1() {
            while (!ready) ; // spin
        }

        @Signal
        public void signal() {
            ready = true;
        }
    }

    @JCStressTest(Mode.Termination)
    @Outcome(id = "TERMINATED", expect = ACCEPTABLE, desc = "Gracefully finished")
    @Outcome(id = "STALE", expect = ACCEPTABLE_INTERESTING, desc = "Test is stuck")
    @State
    public static class VolatileSpin {
        volatile boolean ready;

        @Actor
        public void actor1() {
            while (!ready) ; // spin
        }

        @Signal
        public void signal() {
            ready = true;
        }
    }

}