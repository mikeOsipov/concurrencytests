package ru.ktest.session_provider

import java.util.concurrent.atomic.AtomicInteger

class SessionProviderVolatile : SessionProvider {

    @Volatile
    private var session: Session? = null

    override fun getSession(): Session =
        session ?: requestSession().also { session = it }

    @Volatile
    private var counter = AtomicInteger()
    private fun requestSession(): Session = Session(counter.getAndIncrement())

}