package ru.ktest.session_provider

import java.util.concurrent.atomic.AtomicInteger

class SessionProviderDCL : SessionProvider {

    @Volatile
    private var session: Session? = null

    override fun getSession(): Session =
        session ?: synchronized(this) {
            session ?: requestSession().also { session = it }
        }

    @Volatile
    private var counter = AtomicInteger()
    private fun requestSession(): Session = Session(counter.getAndIncrement())

}