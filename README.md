# Многопоточные тесты на базе [JCStress](https://github.com/openjdk/jcstress) #

Тесты запускаются в рамках gradle проекта, запустить можно командой

> ./gradlew :jcstress --tests "YourTestClassName"

Сами тесты написаны на Java, но в них можно использовать Kotlin-классы из source-папки `kotlin`

### Пакет `ru.test.jcstress.cases`

Тесты с интересными кейсами

### Другие примеры тестов

Примеры со всякими разными тестами можно глянуть в [jcstress/samples](https://github.com/openjdk/jcstress/tree/master/jcstress-samples/src/main/java/org/openjdk/jcstress/samples)

